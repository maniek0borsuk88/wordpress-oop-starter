<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-12">
			<h1>Wordpress OOP Starter</h1>
			<p>Style test</p>
			<span>Style test 2</span>
		</div>
	</div>

    <div class="row">
        <div class="col-8">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Launch demo modal
            </button>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>


            <!--    Loop    -->
            <!--    https://codex.wordpress.org/The_Loop   -->
	        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="card" style="width: 18rem;">
                    <div class="card-img-top img-fluid">
				        <?php the_post_thumbnail('medium'); ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        <p class="card-text"><?php the_content(); ?></p>
                        <a href="<?php the_permalink(); ?>" class="btn btn-primary">Go somewhere</a>
                        <i><?php the_date(); ?> <?php the_time(); ?></i>
                        <span><?php the_tags(); ?></span>
                    </div>
                </div>
	        <?php endwhile; else : ?>

                <p><?php _e('Brak postów do wyświetlenia.','wordpress-oop-starter'); ?></p>

	        <?php endif; ?>

	        <?php echo get_template_part('templates/frontend/posts/template/content', get_post_format()); ?>
        </div>
        <div class="col-4">
	        <?php get_sidebar(); ?>
        </div>
    </div>
</div>



<?php get_footer(); ?>

