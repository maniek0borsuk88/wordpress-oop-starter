<?php get_header(); ?>

<!--    https://codex.wordpress.org/The_Loop   -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="home-page" >
		<?php the_content(); ?>
	</div>
<?php endwhile; else : ?>

	<p><?php _e('Brak postów do wyświetlenia.','wordpress-oop-starter'); ?></p>

<?php endif; ?>


<?php get_footer(); ?>