<?php

namespace namespaceName\main;


class Sidebars {

	public function __construct() {

		add_action('widgets_init', [$this, 'registerSidebarWidget']);

	}

	public function registerSidebarWidget() {

		register_sidebars(4,
			array(
				'class' => 'sidebar-blog-post',
				'description'   => '',
				'before_widget' => '<div><li>',
				'after_widget'  => '</li></div>',
				'before_title'  => '<div><h4>',
				'after_title'   => '</h4></div>'
			));

	}

}