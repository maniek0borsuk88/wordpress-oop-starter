<?php

namespace namespaceName\main;

/**
 *   https://codex.wordpress.org/Navigation_Menus
 */
class Menu {

	public function __construct() {

		add_action( 'after_setup_theme', [$this,'registerMenus'] );

	}

	public function registerMenus() {

		register_nav_menus( [

			'header' => __( 'Header Menu', 'wordpress-oop-starter' ),
			'footer' => __('Footer Menu','wordpress-oop-starter')

		] );

	}

}