<?php
namespace namespaceName\main;


class Support {

	public function __construct() {

		add_theme_support('post-thumbnails');

		add_theme_support( 'custom-background' );

		add_theme_support('post-formats', $this->postFormat() );

		add_theme_support('custom-header', $this->customHeader());

		add_theme_support('custom-logo', $this->customLogo());

		add_theme_support( 'html5', $this->supportHtml5() );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support('woocommerce');

		add_theme_support('widgets');

	}

	public function postFormat() {

		$args = [
			'aside',
			'gallery',
			'image',
			'video',
			'audio',
			'link',
			'quote',
			'status',
			'chat'
		];

		return $args;

	}

	public function customHeader() {

		$defaults = [
			'default-image'          => '',
			'width'                  => 380,
			'height'                 => 280,
			'flex-height'            => true,
			'flex-width'             => true,
			'uploads'                => true,
			'random-default'         => false,
			'header-text'            => true,
			'default-text-color'     => '',
			'wp-head-callback'       => '',
			'admin-head-callback'    => '',
			'admin-preview-callback' => '',
		];

		return $defaults;

	}

	public function customLogo() {

		$args =  [
			'height'      => 100,
			'width'       => 400,
			'flex-height' => true,
			'flex-width'  => true,
		];

		return $args;

	}

	public function supportHtml5() {

		$args = [
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		];

		return $args;

	}


}