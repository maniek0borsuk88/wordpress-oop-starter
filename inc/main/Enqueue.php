<?php
namespace namespaceName\main;

class Enqueue {

	/**
	 * Register scripts and style
	 */
	public function __construct() {

		add_action('wp_enqueue_scripts',[$this, 'enqueueScripts']);

	}


	/**
	 * Method add style and scripts
	 */
	public function enqueueScripts() {

		/* Bootstrap Css */
		wp_enqueue_style('bootstrapMinCss', get_template_directory_uri() . '/css/bootstrap.min.css',[], '4.0','all');

		/* Fontello Css */
		wp_enqueue_style('fontelloCss', get_template_directory_uri() . '/css/fontello/css/fontello.css',[], '1.0','all');

		/* Main Style */
		wp_enqueue_style('mainStyleCss',get_template_directory_uri() . '/css/main.css',[], '1.0', 'all');

		/* Stylesheet */
		wp_enqueue_style('stylesheet', get_stylesheet_uri(), true);

		/* jQuery */
		wp_enqueue_script('jQuery',get_template_directory_uri() . '/js/jquery-3.3.1.min.js',['jquery'],'3.3.1',true);

		/* Bootstrap Js */
		wp_enqueue_script('bootstrapMinJs',get_template_directory_uri() . '/js/bootstrap.min.js',['jquery'],'4.0',true);

		/* Bootstrap Bundle Js */
		wp_enqueue_script('bootstrapBundleMinJs',get_template_directory_uri() . '/js/bootstrap.bundle.min.js',['jquery'],'4.0',true);

		/* Main Js */
		wp_enqueue_script('MainJs', get_template_directory_uri() . '/js/main.js',['jquery'],'1.0',true);

	}

}