<?php
require_once 'conf.php';

if(file_exists(dirname(__FILE__) .  DS . 'vendor' . DS . 'autoload.php')):
	include_once dirname(__FILE__) . DS . 'vendor' . DS . 'autoload.php';
endif;

/**
* Create Instance class Enqueue
 */
if(class_exists('namespaceName\\main\\Enqueue')):
	$enqueue = new \namespaceName\main\Enqueue();
endif;

/**
 * Create Instance class Support
 */
if(class_exists('namespaceName\\main\\Support')):
	$support = new \namespaceName\main\Support();
endif;

/**
 * Create Instance class Sidebars
 */
if(class_exists('namespaceName\\main\\Sidebars')):
	$sidebars = new \namespaceName\main\Sidebars();
endif;

/**
 * Create Instance class Menu
 */
if(class_exists('namespaceName\\main\\Menu')):
	$menuss = new \namespaceName\main\Menu(); // nigdy nie używaj nazwy zmiennej $menu
endif;


if(class_exists('namespaceName\\walker\\WalkerNav')):
	$walker = new \namespaceName\walker\WalkerNav();
endif;


/**
 * @global int $content_width
 */
function oop_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'oop_content_width', 640 );
}
add_action( 'after_setup_theme', 'oop_content_width', 0 );

function oop_enqueue_comments_reply() {
	if( get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'comment_form_before', 'oop_enqueue_comments_reply' );

function oop_editor_style() {
	add_editor_style('css/custom-editor-style.css');
}
add_action('after_setup_theme','oop_editor_style');