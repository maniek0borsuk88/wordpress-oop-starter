<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wordpress-oop-starter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <!--    https://codex.wordpress.org/The_Loop   -->
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div id="home-page" >
		<?php the_content(); ?>
    </div>
	<?php endwhile; ?>


		<!-- Add the pagination functions here. -->

		<div class="nav-previous alignleft"><?php previous_posts_link( 'Older posts' ); ?></div>
		<div class="nav-next alignright"><?php next_posts_link( 'Newer posts' ); ?></div>

		<?php else : ?>
		<p><?php __('Sorry, no posts matched your criteria.','wordpress-oop-starter'); ?></p>
	<?php endif; ?>


</article><!-- #post-<?php the_ID(); ?> -->
